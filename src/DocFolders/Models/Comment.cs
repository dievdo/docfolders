﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DocFolders.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CommentId { get; set; }

        [Required]
        public Guid ItemId { get; set; }
        public virtual Item Item { get; set; }

        [Required]
        public DateTime Date { get; set; }
        [Required]
        [MaxLength(50)]
        public string User { get; set; }
        [Required]
        public string Text { get; set; }
    }
}
