﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DocFolders.Models
{
    public class Link
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid LinkId { get; set; }

        public bool Backward { get; set; } = false;

        [Required]
        public Guid ItemId { get; set; }
        public virtual Item Item { get; set; }
    }
}
