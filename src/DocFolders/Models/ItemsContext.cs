﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;

namespace DocFolders.Models
{
    //cd %USERPROFILE%
    //dnvm install 1.0.0-rc1-final
    //dnvm use 1.0.0-rc1-final

    //cd \Repos\Project\src\Project
    //dnx ef migrations add FirstMigration
    //dnx ef database update

    //(see DB in %USERPROFILE%)

    public class ItemsContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Link> Links { get; set; }
    }
}
