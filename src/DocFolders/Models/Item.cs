﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DocFolders.Models
{
    public class Item
    {
        public enum ItemType
        {
            Unknown = 0,
            Root,
            Folder,
            Draft,
            Document
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ItemId { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public ItemType Type { get; set; } = 0;

        public string Title { get; set; }
        public string Content { get; set; }

        public string Source { get; set; }

        [Required]
        [MaxLength(50)]
        public string CreateUser { get; set; }
        [Required]
        public DateTime CreateDate { get; set; }

        [MaxLength(50)]
        public string UpdateUser { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();
        public virtual ICollection<Link> Links { get; set; } = new List<Link>();
    }
}
