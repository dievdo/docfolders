using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DocFolders.Models;

namespace DocFolders.Migrations
{
    [DbContext(typeof(ItemsContext))]
    [Migration("20160411143417_FirstMigration")]
    partial class FirstMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DocFolders.Models.Comment", b =>
                {
                    b.Property<Guid>("CommentId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<Guid>("ItemId");

                    b.Property<string>("Text")
                        .IsRequired();

                    b.Property<string>("User")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("CommentId");
                });

            modelBuilder.Entity("DocFolders.Models.Item", b =>
                {
                    b.Property<Guid>("ItemId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("CreateUser")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Source");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.Property<DateTime?>("UpdateDate");

                    b.Property<string>("UpdateUser")
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("ItemId");
                });

            modelBuilder.Entity("DocFolders.Models.Link", b =>
                {
                    b.Property<Guid>("LinkId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Backward");

                    b.Property<Guid>("ItemId");

                    b.HasKey("LinkId");
                });

            modelBuilder.Entity("DocFolders.Models.Comment", b =>
                {
                    b.HasOne("DocFolders.Models.Item")
                        .WithMany()
                        .HasForeignKey("ItemId");
                });

            modelBuilder.Entity("DocFolders.Models.Link", b =>
                {
                    b.HasOne("DocFolders.Models.Item")
                        .WithMany()
                        .HasForeignKey("ItemId");
                });
        }
    }
}
