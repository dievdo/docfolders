﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using System.IO;
using DocFolders.ViewModels;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DocFolders.Controllers
{
    public class MyDocumentsController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            //string userDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string userProfile = Environment.GetEnvironmentVariable("USERPROFILE");
            string path = Path.Combine(userProfile, "Documents");

            FileFolder ff = new FileFolder(path);
            return View(ff);
        }

        private void FileSearch(string Dir)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(Dir);
            foreach(DirectoryInfo subDirInfo in dirInfo.GetDirectories())
            {
                FileSearch(subDirInfo.FullName);
            }
            foreach(FileInfo fileInfo in dirInfo.GetFiles())
            {
                //FileList
            }
        }
    }
}
