﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using System.IO;
using DocFolders.ViewModels;

namespace DocFolders.ViewComponents
{
    public class FilesView : ViewComponent
    {
        public FilesView()
        { }

        public async Task<IViewComponentResult> InvokeAsync(FileFolder ff)
        {
            return View(ff.Files);
        }
    }
}