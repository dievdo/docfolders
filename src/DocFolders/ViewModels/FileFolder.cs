﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace DocFolders.ViewModels
{
    public class FileFolder
    {
        public string[] Folders { get; set; }
        public string[] Files { get; set; } 

        public FileFolder()
        { }

        public FileFolder(string path)
        {
            //TODO Skip hidden files and "~$*.docx"

            Folders = Directory.GetDirectories(path);
            Files = Directory.GetFiles(path);
        }
    }
}
