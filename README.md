# DocFolders
A company information system to handle with paper documents originally placed in file folders.

Created to try new possibilities of ASP.NET 5 (Core) and MVC 6 in Microsoft Visual Studio Community 2015 with its integrity to GitHub, and to try to integrate this git repository with BitBucket too.
 
## Questions
Currently I have more questions myself for ways of implementing that.

## Copyright and License 
Licensed under the [CC0 1.0 Universal](/LICENSE) (the "License"); you may not use this software except in compliance with the License.
Unless expressly stated otherwise, the person who associated a work with this deed makes no warranties about the work, and disclaims liability for all uses of the work, to the fullest extent permitted by applicable law. When using or citing the work, you should not imply endorsement by the author or the affirmer.

## More Information 
To find out more, please visit the [DocFolders](http://diev.github.io/DocFolders) page.
